from django.apps import AppConfig


class PorteriaConfig(AppConfig):
    name = 'porteria'
