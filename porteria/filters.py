import django_filters
from porteria.models import Control, Persona
from django import forms
from django.db import models
from django_filters.constants import EMPTY_VALUES

class TestFilter(django_filters.DateFilter):
    def filter(self, qs, value):
        if value in EMPTY_VALUES:
            return qs
        if self.distinct:
            qs = qs.distinct()
        #lookup = '%s__%s' % (self.field_name, self.lookup_expr)
        lookup = 'fecha__date'
        print("lookup", lookup)
        print("value", type(value))
        print(qs)
        print(value)
        #qs = self.get_method(qs)(**{lookup: value})
        return qs.filter(fecha__date=value)
        return qs


class ControlFilter(django_filters.FilterSet):
    persona__rut = django_filters.CharFilter(widget=forms.TextInput(attrs={'class': 'form-control rut'}))
    persona__apellido1 = django_filters.CharFilter(widget=forms.TextInput(attrs={'class': 'form-control'}))
    temperatura = django_filters.CharFilter(widget=forms.NumberInput(attrs={'class': 'form-control','max' : '43'}))
    fecha = django_filters.DateFilter(lookup_expr='date', widget=forms.TextInput(attrs={'class': 'form-control datetimepicker'}))
    class Meta:
        model = Control
        fields = {
            'fecha': ['contains'],
            'temperatura': ['exact'],
            'persona__rut':['exact', 'contains'],
            'persona__apellido1': ['exact'],
        }
		
