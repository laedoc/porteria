from django.forms import ModelForm
from django import forms
from porteria.models import *
from django.contrib.auth.forms import UserCreationForm, AuthenticationForm , UsernameField
from django.contrib.auth.models import User
from django.utils.translation import gettext, gettext_lazy as _

class MyAuthenticationForm(AuthenticationForm):
	username = UsernameField(widget=forms.TextInput(attrs={'autofocus': True,'class':'form-control'}))
	password = forms.CharField(
		label=_("Password"),
		strip=False,
		widget=forms.PasswordInput(attrs={'autocomplete': 'current-password','class':'form-control'}),
    )


class PersonaForm(forms.ModelForm):
	rut = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control rut'}))
	nombre = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	apellido1 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	apellido2 = forms.CharField(widget=forms.TextInput(attrs={'class': 'form-control'}))
	procedencia = forms.ChoiceField(choices=origen, widget=forms.Select(attrs={'class':'form-control'}))
	temperatura = forms.FloatField(required=False,widget=forms.NumberInput(attrs={'class': 'form-control','max' : '43'}))

	class Meta:
		model = Persona
		exclude = [""]

	def save(self, *args, **kwargs):

		if Persona.objects.filter(rut=self.cleaned_data.get('rut')).exists() == False:
			persona = super(PersonaForm, self).save(*args,**kwargs)
		else:
			persona = Persona.objects.get(rut=self.cleaned_data.get('rut'))

		temperatura = self.cleaned_data.get('temperatura')
		if temperatura:
			Control.objects.create(
				persona=persona,
				temperatura=self.cleaned_data.get('temperatura'))
		return persona



class ControlForm(forms.ModelForm):
	persona = forms.ModelChoiceField(
        queryset=Persona.objects.all(),
        widget=forms.Select(attrs={'class': 'form-control selectpicker', 'data-style': 'btn btn-link' })
    )
	temperatura = forms.FloatField(required=False,widget=forms.NumberInput(attrs={'class': 'form-control','max' : '43'}))

	class Meta:
		model = Control
		exclude = [""]


class SalidaForm(forms.ModelForm):
	placeholder = 'Seleccione un ingreso'
	salida = Salida.objects.values_list("control")
	control = forms.ModelChoiceField(
		queryset=Control.objects.filter().exclude(id__in=salida),
		empty_label=placeholder,
		widget=forms.Select(attrs={'class': 'form-control selectpicker', 'data-style': 'btn btn-link' })
		)

	class Meta:
		model = Salida
		exclude = [""]




