from django.db import models
from django.template.defaultfilters import date

# Create your models here.


origen=(
	("S","Persona Externa" ),
	("N", "Funcionario")
	)
class Persona(models.Model):
    rut = models.CharField(max_length=32)
    nombre = models.CharField(max_length=32)
    apellido1 = models.CharField(max_length=32)
    apellido2 = models.CharField(max_length=32)
    procedencia = models.CharField(max_length=1,choices=origen,default="S")

    class Meta:
            ordering = ('apellido1',)

    def __str__(self):
        # __unicode__ on Python 2 -->  __str__ en Python 3

        return f'{self.apellido1} {self.nombre}'
        # return u"%s %s"% (self.apellido1,self.nombre)

class Control(models.Model):
    persona = models.ForeignKey(Persona, on_delete=models.PROTECT)
    temperatura = models.FloatField(null=True,blank=True)
    fecha = models.DateTimeField(auto_now_add=True,null=True,blank=True)

    def __str__(self):
         return f'{self.persona}  {date(self.fecha, "d-m-Y H:i:s")} '


class Salida(models.Model):
    control = models.ForeignKey(Control,on_delete=models.PROTECT)
    fecha = models.DateTimeField(auto_now_add=True,null=True,blank=True)