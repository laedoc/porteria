from django.shortcuts import render,redirect
from django.contrib.auth.views import LoginView,LogoutView
from django.contrib.auth.mixins import LoginRequiredMixin
from django.views.generic.edit import CreateView
from django.views.generic.list import ListView
from django.views.generic.detail import DetailView
from django.urls import reverse, reverse_lazy, resolve
from porteria.forms import *
from porteria.models import *
from django.utils import timezone
from porteria.filters import ControlFilter
# Create your views here.
class MyLoginView(LoginView):
    form_class = MyAuthenticationForm


class MyLogoutView(LogoutView):
    pass


class PersonaFormView(LoginRequiredMixin,CreateView):
    form_class = PersonaForm
    template_name = 'index.html'
    model = Persona

    success_url = reverse_lazy('controlfilter')

    def get_context_data(self, *args, **kwargs):
        context = super(PersonaFormView, self)\
            .get_context_data(*args, **kwargs)
        context['hoy'] = timezone.now()
        return context

class ControlFilterView(LoginRequiredMixin,ListView):
	model = Control
	template_name = "porteria/controlfiltro.html"

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['hoy'] = timezone.now()
		context['filter'] = ControlFilter(self.request.GET, queryset=self.get_queryset())
		context['salidas'] = Salida.objects.all()
		return context

class SalidaFormView(LoginRequiredMixin,CreateView):
	form_class = SalidaForm
	template_name = "porteria/salida.html"
	model = Salida
	success_url = reverse_lazy('controlfilter')

	def get_context_data(self, **kwargs):
		context = super().get_context_data(**kwargs)
		context['hoy'] = timezone.now()
		return context
